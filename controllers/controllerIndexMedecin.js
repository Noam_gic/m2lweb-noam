const pg = require('pg');

/* GET index connexion Page*/
exports.connexionMedecin = function(req, res, next) {
    res.render('connexion', { title: 'Authentification'});
};

/* Page de vérification suite à la connexion*/
exports.verificationConnexionMedecin = function(req, res, next) {

    //DEBUG console.log(req.body.email);

    const connectionString = 'postgres://tarocode:tarocode@195.221.64.108:5432/M2L';

    const db = new pg.Client(connectionString);
    db.connect();

    const query = {
        name: 'fetch-one-senior',
        text: 'SELECT * FROM medecin WHERE Email = $1 and Password=$2',
        values: [req.body.email,req.body.password]
    };

    db.query(query,
        function(err, result){
            if (err) {
                console.log(err.stack);
                res.send('ERROR');

            } else {
                if (result.rows.length == 0)
                {
                    res.redirect('/');
                }
                else
                {
                    req.session.user = result.rows[0];
                    //DEBUG console.log(req.session.user);
                    res.render('index',{user:req.session.user});
                }
            }
            db.end();
        }
    );

    // Code à compléter : implémenter la vérification des informations de connexion
};


/* GET index home Page*/
exports.index = function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
        res.render('index', { title: 'Bienvenue sur le site de la M2L',user:req.session.user});
};

/* GET Déconnexion */
exports.deconnexion = function(req, res, next) {
    req.session.user = undefined;
    res.redirect('/');
};

/* GET Profile User*/
exports.getProfilUser = function(req, res, next) {
    if (req.session.user === undefined)
        res.redirect('/');
    else
        res.render('profile',{listeActivitesInscrit: result.rows,user:req.session.user});
};