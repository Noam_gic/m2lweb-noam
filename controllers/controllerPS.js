const pg = require('pg');

/*GET Liste des Activitées*/
exports.getAllActivites=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString ='postgres://tarocode:tarocode@195.221.64.108:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();


        const query = {
            name: 'fetch-all-activite',
            text: 'SELECT * FROM activite'
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('activite', {listeDesActivites: result.rows,user:req.session.user});
                }
                db.end();
            }
        );
    }
};

/*GET liste des activitées inscrite*/
exports.getActivitesInscrit=function(req, res, next) {
    if (req.session.user === undefined)
        res.redirect('/');
    else {
        const connectionString = 'postgres://tarocode:tarocode@195.221.64.108:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();


        const query = {
            name: 'fetch-all-activite-inscrit',
            text: "SELECT distinct a.* FROM activite a left outer join seance s on s.idact = a.identifiant join participer  p on p.codeseance = s.code join senior se on se.numsecu = p.numsecu where se.nom like $1 ",
            values: [req.session.user.nom]
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('profile', {listeActivitesInscrit: result.rows, user: req.session.user});
                }
                db.end();
            }
        );
    }
};

/*POST Ajout d'un sénior à une activité*/
exports.addOneSeniorByActivite=function(req, res, next) {
    if (req.session.user === undefined)
        res.redirect('/');
    else {
        const connectionString = 'postgres://tarocode:tarocode@195.221.64.108:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();

        const query = {
            name: 'insert-one-senior-by-activities',
            text: "INSERT into like $1 ",
            values: [req.session.user.nom]
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.redirect('/activites');
                }
                db.end();
            }
        );
    }
};