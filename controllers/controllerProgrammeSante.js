const pg = require('pg');

exports.getAllSeanceActivitesInscrit=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString ='postgres://tarocode:tarocode@195.221.64.108:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();

         console.log(req.session.user['numsecu']);
        const query = {
            name: 'fetch-all-seance',
            text: 'select dateseance,designation from senior s\n' +
            'inner join participer p\n' +
            'on s.numsecu=p.numsecu\n' +
            'join seance se\n' +
            'on se.code=p.codeseance\n' +
            'join activite a \n' +
            'on se.idact=identifiant\n' +
            'where s.numsecu = $1',
            values: [req.session.user['numsecu']]
        };

        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('programmeSante', {listeDesSeancesActivites: result.rows,user:req.session.user});
                }
                db.end();
            }
        );
    }
};