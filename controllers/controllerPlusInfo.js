const pg = require('pg');

/*GET Liste des Activitées*/
exports.getAllSeanceDuneActivite=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        console.log(req.params.identifiant);
        const connectionString ='postgres://tarocode:tarocode@195.221.64.108:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();


        const query = {
            name: 'fetch-all-seance',
            text: 'select codeseance, dateseance from senior s\n' +
            'inner join participer p\n' +
            'on s.numsecu=p.numsecu\n' +
            'join seance se \n' +
            'on se.code=p.codeseance\n' +
            'join activite a \n' +
            'on a.identifiant=se.idact\n' +
            'where a.identifiant= $1 and s.numsecu = $2',
            values:[req.params.identifiant, req.session.user['numsecu']]
        };console.log(query.values);


        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('plusInfo', {listeDesSeancesDuneActivite: result.rows,user:req.session.user});
                    //console.log(result.rows)
                }
                db.end();
            }
        );


            }
};
exports.deleteUneSceance=function(req, res, next) {

    if (req.session.user === undefined)
        res.redirect('/');
    else
    {
        const connectionString ='postgres://tarocode:tarocode@195.221.64.108:5432/M2L';

        const db = new pg.Client(connectionString);
        db.connect();
console.log(req.body.inscription);
        const query= {
            name:'delet-une-seance',
            text:'delete from participer  where  codeseance =$1 and numsecu=$2 ',
            values:[req.body.inscription, req.session.user['numsecu'] ]

        };console.log(query.values);
        db.query(query,
            function (err, result) {
                if (err) {
                    console.log(err.stack);
                    res.send('ERROR BD');

                } else {
                    res.render('plusInfo', {listeDesSeancesDuneActivite: result.rows,user:req.session.user});
                    console.log(result.rows)
                }
                db.end();
            }
        );
    }
};

