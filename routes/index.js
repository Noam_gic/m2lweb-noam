var express = require('express');
var router = express.Router();

var controllerIndex = require("../controllers/controllerIndex");
var controllerIndexMedecin = require("../controllers/controllerIndexMedecin");
var controllerActivite = require("../controllers/controllerPS");
var controllerProgrammeSante= require("../controllers/controllerProgrammeSante");
var controllerPlusInfo= require("../controllers/controllerPlusInfo");


/* GET Connexion page. */
router.get('/', controllerIndex.connexion);
router.post('/', controllerIndex.verificationConnexion);

/* Connexion en tant que medecin */
router.get('/connexionMedecin', controllerIndexMedecin.connexionMedecin);
router.post('/connexionMedecin', controllerIndexMedecin.verificationConnexionMedecin);


router.get('/logout', controllerIndex.deconnexion);

router.get('/index', controllerIndex.index);

/*router.get('/profile', controllerIndex.getProfilUser);*/




/* ACTIVITES*/
router.get('/activites',controllerActivite.getAllActivites);
router.get('/profile',controllerActivite.getActivitesInscrit);
router.post('/activites',controllerActivite.addOneSeniorByActivite);



/* Programme Santé */

router.get('/programmeSante',controllerProgrammeSante.getAllSeanceActivitesInscrit);

/*Programme sante plus d'info*/

router.get('/activites/:identifiant/seances',controllerPlusInfo.getAllSeanceDuneActivite);
router.get('/plusInfo/',controllerPlusInfo.getAllSeanceDuneActivite);
router.post('/plusInfo/',controllerPlusInfo.deleteUneSceance);

module.exports = router;
